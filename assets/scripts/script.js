
		// types of comments
		// 1) single-line comment = //
		/*2) multi-line comment =*/ /**/

		//statements
		// instructions that will tell the computer to perform certain actions
		//javascript statements end in semicolon (;)

		//syntax
		//set of rules describes how statements are constructed
		//container for data
		//named location for a stored value
		let product = "computer";
		let price = 15000;
		const pi = 3.1416;

		// multiple variable declaration
		let code = "12345", brand = "dell"
		const number = 2, text = "hello"

		// declaring a variable without value
		let empty;

		// Using the console object
		console.log('Hello World');
		console.warn('You have been warned!');
		console.error('Error 404');

		// Using variables in statements
		console.log(product);
		console.log(product, price);

		// Using a variable that doesn't exist
		// console.log(bigfoot);

		// // Using reserved keywords
		// let let = "test";

		// Data Types
		// 1. Strings
		let country = 'Philippines';
		let province = 'Metro Manila';
		let address = province + country; // concatenation
		console.log(address);

		// 2. Numbers
		let grade = 98.9;
		let headcount = 26;
		let distance = 2e10;
		let formula = 2 + 10;
		console.log(formula);

		// 3. Boolean
		let isRich = false;
		let isHandsome = true;

		let myMathGrade = 99.99;

		// naming convention
		// standard way of naming in programming

		// camel case
		// isRich

		// kebab case
		// is-rich

		// snake case
		// is_rich

		// 4. Array
		// a colelction of data
		let grades = [93.1, 95.2, 96.3];

		console.log(grades)
		let random = ["hi", 10, false];
		console.log(random);

		// 5. Objects
		let person = {
			name: 'Juan Tamad',
			isMarried: false,
			age: 23,
			handsome: isHandsome,
			address: {
				barangay: 'San Juan',
				city: 'Manila',
				region: 'NCR'
			}
		};

		/*let objectName = {
			// key value pair
			key: value
		};*/

		// Null
		// absence of a value in variable declaration
		let spouse = null;
		console.log(spouse);

		// Undefined
		// unassigned value
		let fullName;
		console.log(fullName);

		// Basic Assignment Operator (=)
		let total = 1;

		// Addition Assignment Operator
		total = total + 2;
		console.log(total); //3
		total += 2;
		console.log(total); //5

		// Increment
		let x = 0;
		let increment = ++x;
		console.log("increment:" + increment);

		// Decrement
		let y = 1;
		let decrement = --y;
		console.log("decrement:" + decrement);

		// Arithmetic Operators

		let a = 1;
		let b = 2;

		// Sum
		let sum = a + b;
		console.log("Sum:" + sum); //3

		// Difference
		let difference = b - a;
		console.log("Difference:" + difference); //1

		let multiply = a * b;
		let quotient = b / a;

		// modulo
		let remainder = b % a;
		console.log("Remainder: " + remainder);

		// Increment
		// let x = 0;
		// let increment = ++x;
		// console.log("increment:" + increment);

		// Decrement
		// let y = 1;
		// let decrement = --y;
		// console.log("decrement:" + decrement);

		// Type Coercion
		let coercion = 12.3 + 5;
		console.log(coercion.toFixed(4));

		// Convert to decimal
		parseFloat(coercion);
		console.log(coercion);

		// Convert to integer
		parseInt(coercion);
		console.log(coercion);

		// Comparison Operators

		let comparison = "1";
		let newName = 'juan';

		// Equality Operator (==)
		console.log(1 == comparison);
		console.log(1 == 2);
		console.log('juan' == newName)
		console.log('juan' == 'juan');
		console.log(0 == false);

		// Inequality Operator (!=)
		console.log(1 != 1);
		console.log('juan' != newName);
		console.log(1 != 2);

		// Strict Equality Operator
		console.log(1 === comparison);
		console.log('juan' === newName);

		// Strict Inequality Operator (!==)
		console.log(1 !== comparison);
		console.log('juan' !== newName);

		// Relational Operators (>, <)
		let first = 5;
		let second = 10;

		console.log(first > second);
		console.log(first < second);

		// Greater than OR equal to (>=)
		console.log(10 >= first); // true
		console.log(10 <= second); // true

		// Less than OR equal to (<=)
		console.log(10 <= first); // false
		console.log(10 <= second); // true

		//Logical Operators (&& = and, || = or)
		// && - all values have to be true to result true value
		let isLegalAge = true;
		let isRegistered = false;

		let bothTrue = isLegalAge && isRegistered;
		console.log(bothTrue)
		let allTrue = isLegalAge && isRegistered && isSmart;
		console.log(allTrue);

		// || - one data must return true
		let oneTrue = isLegalAge || isRegistered;
		console.log(oneTrue); // true
		let notTrue = !isLegalAge || isRegistered
		console.log(notTrue);

		// Functions
		/*functions functionName (parameters) {
			// code block
		}*/

		function createFullName (firstName, lastName) {
			console.log(firstName + '' + lastName)
		};

		// let stringone = 'Arvin' + '' + 'Lacdao';
		// console.log(stringone);
		// let stringtwo = 'Juan' + '' + 'Tamad';
		// console.log(stringtwo);

		createFullName('Arvin', 'Lacdao');
		createFullName('Juan', 'Tamad');

		function printString (text) {
			return text
		
		};

		let myName = printString('Arvin');

		function functionName (parameters) {
			//code block goes
		}

		functionName(arguments)